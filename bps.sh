#!/bin/bash
# bps - bash podcatching software
# http://gitlab.com/pavelgoriacko/bps
# Licensed under GPLv2. See LICENSE for details. 

## datadir is the directory you want podcasts saved to:
datadir=/mnt/hdd/music/Podcasts

## Main functions
function main {
    case $1 in
        "download")
            rm new.log

	    # Use 'new.log' to collect new episode data
            download new.log true
	    
            # Move dynamically created log file to permanent log file:
            cat new.log > temp.log
            cat podcast.log >> temp.log
            sort temp.log | uniq > podcast.log
            rm temp.log
            ;;
	"mark")
	    # Move dynamically created log file to permanent log file:
            cat test.log > temp.log
            cat podcast.log >> temp.log
            sort temp.log | uniq > podcast.log
            rm temp.log
	    printf "All new episodes marked as read."
            ;;
        "test")
            download test.log false
	    # Display the list of available episodes:
	    new_episodes=$(grep "," new_episodes.log | wc -l)
	    if (($new_episodes == 0)) ; then
		printf "\n\nNo new episodes available."
	    else
		printf "\n\n$new_episodes new episodes available:\n"
		awk '{count[$1]++}END{for(j in count) print j,"("count[j]" episodes)"}' FS=, new_episodes.log
	    fi
            ;;
        "help" | "usage" | *)
            usage;;
    esac
}

function usage {
    echo "Usage: bps.sh download | test | mark | help"
}

function produce_filename {
    url=$1
    filename=$(echo $url | awk -F'/' '{print $NF}' | awk -F'?' {'print $1'})
    echo $filename
}

function download {
    logfile=$1
    download_media=$2

    # Make script crontab friendly:
    cd $(dirname $0)

    # Display logo
    cat logo.txt

    # Create datadir if necessary:
    if $download_media ; then
        mkdir -p $datadir
    fi
    touch podcast.log

    # Delete any temp file:
    rm -f $logfile
    touch $logfile

    # Delete new episode file
    rm -f new_episodes.log
    touch new_episodes.log

    # Create new playlist
    if $download_media; then
        printf "\nCreating playlist... "
        cat $datadir/latest10.m3u > $datadir/latest.m3u
        printf "Done.\n"
    fi

    ## Read the bp.conf file and curl files
    ## Only for files not in the podcast.log file
    ## Only if ``download_media`` is true:

    # Skip any lines not starting with a comment (#)
    grep "^[^#;]" bp.conf |
	
    while read podcastfields
        do
        podcast=$(echo $podcastfields | cut -d',' -f1)
        dname=$(echo $podcastfields | cut -d',' -f2)
	dlnum=$(echo $podcastfields | cut -d',' -f3)
	counter=1

	## Create a directory structure
        mkdir -p "$datadir/$dname"

	## Check the podcast for new episodes
	printf "\n\nChecking $dname..."
	## Use curl to download rss feed
	## Then pipe into xsltproc to obtain desired fields
	curl --silent -L $podcast | xsltproc parse_enclosure.xsl - |
	while read episodes
	do
	    # For each episode, produce url, name, date, and filename variables
	    url=$(echo $episodes | cut -d'|' -f2)
    	    epname=$(echo $episodes | cut -d'|' -f1)
	    date=$(echo $episodes | cut -d'|' -f3)
	    filename=$(produce_filename $url)
	    
	## Check against log of downloaded episodes
	if [[ -z $(grep "$epname" podcast.log) ]] ; then
		
		# If not in the log, add the file to the download queue
                echo $epname >> $logfile
		
		# Add podcast episode into "new episodes" file
		printf "$dname,$epname\n" >> new_episodes.log
		
		# If "download" function is on, start downloading episodes
        	if $download_media ; then

		    # Stop downloading after a prespecified amount of episodes
		    if (( "$dlnum" >= "$counter" )) ; then

		    # Download podcast files
		    printf "\nDownloading ''$epname'' ...\n"
		    
		    # Fetch the file using curl with progress bar
		    curl -t 10 --progress-bar -L -o "$datadir/$dname/$filename" "$url"	
		    printf "Done."

		    # Write id3vs tags (name, artist, year, date)
		    printf "\nWriting tags... "
		    id3v2 -a "$dname" -t "$epname" --TDAT $(date -d "$date" +%d%m) -y $(date -d "$date" +%Y) "$datadir/$dname/$filename"
		    printf "Done."

		    # Rename file as "yyyy-mm-dd epname.mp3"
		    printf "\nRenaming..."
		    newfilename="$datadir/$dname/$(date -d "$date" +%Y-%m-%d) $epname.mp3"
		    mv "$datadir/$dname/$filename" "$newfilename"
		    printf "Done."
		    
 		    # Add the episode to the "total downloads" counter
		    ((counter=counter+1))

		    # Create entries for latest podcasts in the playlist
		    printf "\nAdding to playlist... "
                    echo "$newfilename" >> $datadir/latest.m3u
		    printf "Done."
		
		fi
		
                fi
            fi
            done
        done 

    # Leave only the 10 latest podcasts in the global m3u
    if $download_media ; then
	printf "\nLeaving the latest 20 episodes in the playlist... "
	cat $datadir/latest.m3u | tail -n 20 > $datadir/latest10.m3u
	rm -f $datadir/latest.m3u
	printf "Done."
    fi

    ## Note: make this a separate function
    ## Update mpd database (uncomment if running mpd)
    if $download_media; then
    	printf "\n\nUpdating mpd database...\n"
    	mpc -q update
    	printf "Done.\n"
    fi
}

main $@

printf "\n"
