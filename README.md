# bps - bash podcatching software

**bps** is a GNU bash script to download and organize your podcast collection. Originally forked from [bashpodder](http://lincgeek.org/bashpodder) podcatching script, this version focuses on organizing the podcast collection and making sure all files are properly tagged.

## Features

  * Scan podcast RSS feeds and download the latest episodes
  * Tag podcats files and place in a neat folder structure
  * Create a playlist with latest available episodes
  * Optionally update mpd database upon run completion

## Requirements

The following packages are required to run bps:

  * curl
  * xsltproc
  * standard GNU tools (awk, sed, cut, date)
  * id3v2

## Usage

  1. Edit line 7 of the ```bps.sh``` file to set your preferred podcast directory (defaults to a folder in current directory)
  2. Add your podcast feed urls to the ```bp.conf``` file using the format of ```[url],[name],[episodes to download]```.
  2. View available new episodes using ```./bps.sh test```
  3. Download new episodes using ```./bps.sh download```
  4. To debug, executing via ```bash -x bps.sh test &> debug.log``` will create a log file for debugging. 

## License

Licensed under [GNU General Public License, version 2](http://www.gnu.org/licenses/gpl-2.0.html). 
